package br.nom.belo.marcio.agendatel.controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
 
public class TabelaFuncs {
	
	private static String driver = "com.mysql.cj.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/agendatel";
	private static String username = "admin";
	private static String password = "4poa";
 
	public static Connection getConnection(){
		Connection conn = null;
		try{
			Class.forName(driver);
			conn = DriverManager.getConnection(url, username, password);		
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	public static void insert(String nome, String telefone){
		//adiciona contatos ao banco de dados por meio de um prepared statement
		try{			
			Connection conn = getConnection();
			String insert = "INSERT INTO CONTATOS (Nome, Telefone) values (?, ?)";
			PreparedStatement insertSt = conn.prepareStatement(insert);
            insertSt.setString(1, nome);
            insertSt.setString(2, telefone);
            insertSt.execute();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void remove(String nome, String telefone) {
		//remove todos os contatos com a combinaçao de nome e tel 
		try{			
			Connection conn = getConnection();
			String delete = "DELETE FROM CONTATOS WHERE NOME=? AND TELEFONE=?";
			PreparedStatement deleteSt = conn.prepareStatement(delete);
            deleteSt.setString(1, nome);
            deleteSt.setString(2, telefone);
            deleteSt.execute();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void update(String nome, String telefone, String n, String t) {
		//atualiza todos os contatos com a combinaçao de nome e tel
		try{			
			Connection conn = getConnection();
			String update = "UPDATE CONTATOS SET NOME=?, TELEFONE=? WHERE NOME=? AND TELEFONE=?";	
			PreparedStatement updateSt = conn.prepareStatement(update);
			updateSt.setString(1, n);
            updateSt.setString(2, t);
            updateSt.setString(3, nome);
            updateSt.setString(4, telefone);
            updateSt.execute();
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}