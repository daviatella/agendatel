package br.nom.belo.marcio.agendatel.controle;
import java.sql.*;
import br.nom.belo.marcio.agendatel.modelo.Contato;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class TabelaAgendaModelo extends AbstractTableModel {

    private List<Contato> lista=new ArrayList<Contato>();

    public TabelaAgendaModelo() {
    }

    public Contato getContato(int linha) {
        return lista.get(linha);
    }
    
    @Override
    public int getRowCount() {
        if(lista!=null) return lista.size();
        else return 0;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0: 
                return "Nome";
            case 1:
                return "Telefone";
            default:
                return null;
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Contato contato=lista.get(rowIndex);
        switch(columnIndex) {
            case 0:
                return contato.getNome();
            case 1:
                return contato.getTelefone();
            default:
                return null;
        }
    }

    public void setContato(Contato contatoEditado, int linha) {
        lista.set(linha, contatoEditado);
        fireTableDataChanged();
    }
    

    public void adicionarContato(Contato contato) {
    	TabelaFuncs.insert(contato.getNome(), contato.getTelefone());
        lista.add(contato);
        fireTableDataChanged();
    }
  
    void atualizarContatos(int linha, String n, String t) {
    	String nome = lista.get(linha).getNome();
    	String telefone = lista.get(linha).getTelefone();
    	TabelaFuncs.update(nome, telefone, n, t);
        fireTableDataChanged();
    }
    
    public void removerContato(int linha) {
    	String nome = lista.get(linha).getNome();
    	String telefone = lista.get(linha).getTelefone();
    	TabelaFuncs.remove(nome, telefone);
        lista.remove(linha);
        fireTableDataChanged();
    }
   
    void carregarContatos() {
    	//carrega todos os contatos a um resultset e os adiciona a lista um por um
    	try {
	        Connection conn = TabelaFuncs.getConnection();
	        Statement st = conn.createStatement();
	        String q = "select * from contatos"; 
	
	        ResultSet data = st.executeQuery(q);
	        while(data.next()) {
	        	Contato con = new Contato(data.getString("Nome"),data.getString("Telefone"));
	            lista.add(con);
	            fireTableDataChanged();
	        }
	        conn.close();      
		} catch(Exception e){
			 JOptionPane.showMessageDialog(null, e);
	    }
    }
}
	